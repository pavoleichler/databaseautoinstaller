<?php

namespace PavolEichler;

/**
 * Executes the SQL queries as they are appended to a provided SQL file.
 * Useful to automatically run SQL install scripts containing the DB structure.
 */
class DatabaseAutoinstaller {

    protected $dibi;
    protected $environment;
    protected $silent;

    protected $storageFile = 'database-autoinstaller_status.json';
    protected $storage;

    protected $structureFile;

    protected $status = null;
    protected $firstInstall = false;

    /**
     * Executes the SQL queries as they are appended to a provided SQL file.
     *
     * @param \Dibi\Connection $dibi Dibi connection object
     * @param string $storage Path to a storage directory to be used by the object.
     * @param string $structureFile Path to a SQL file to monitor and execute queries from.
     * @param string $environment A unique name for the envirnonment the application is currently used on. Useful to distinguish multiple envrionments, e.g. produciton / test server.
     * @param boolean $silent Optional. If set to true, a PHP warning is issued on SQL error, otherwise an exception is throwned. Default true.
     */
    public function __construct(\Dibi\Connection $dibi, $storage, $structureFile, $environment = 'production', $silent = true) {
        
        $this->dibi = $dibi;
        $this->environment = $environment;
        $this->silent = $silent;
        $this->storage = $storage;
        $this->structureFile = $structureFile;

    }

    /**
     * Get all SQL queries from the structure file.
     *
     * @return string
     */
    public function getAllSql() {

        return file_get_contents($this->structureFile);

    }

    /**
     * Get only SQL that was not yet executed.
     *
     * @return string
     */
    public function getQueuedSql() {

        // load installation status from previous calls, if any
        $this->loadStatus();

        // get all newly appended queries
        $sql = $this->getStructureDifference();

        // get a list of queries and their byte offsets
        $queries = $this->parseQueries($sql);

        $implode = "";
        foreach($queries as $query)
            $implode .= $query['sql'];

        return $implode;

    }

    /**
     * Retrieves the last update time.
     *
     */
    public function getLastUpdateTime() {

        $this->loadStatus();

        return $this->status['last_update'];

    }

    /**
     * Reset status. This will execute the whole structure file on the next install() call.
     */
    public function resetStatus() {

        $this->updateStatus(0);

    }

    /**
     * Set status as 'all queries executed'.
     */
    public function statusComplete() {

        $this->updateStatus();

    }

    /**
     * Check for changes in the install SQL script and execute them.
     *
     * @return boolean True if changes were executed sucesfully, false on error or no changes.
     * @throws Exception
     */
    public function install() {

        // load installation status from previous calls, if any
        $status = $this->loadStatus();

        // no new statements
        if (!$this->structureHasChanged())
            return false;

        // new statements exist, we are goning to write to the DB
        // lock the status file exclusively
        $this->lockStorageFile();

        // reload the status
        $this->loadStatus();
        // if the status has changed already, another thread is already making the changes, do nothing
        if ($status['bytes'] != $this->status['bytes'] OR
            $status['last_update'] != $this->status['last_update']){
            $this->unlockStorageFile();
            return false;
        }

        // get all newly appended queries
        $sql = $this->getStructureDifference();

        // do we have any new SQL queued?
        if ($sql){

            // get a list of queries and their byte offsets
            $queries = $this->parseQueries($sql);

            // execute the queries, if we have found any new SQL
            $this->executeQueries($queries);

        }else{

            // we do not, look like the file has shrinked or received some new whitespace
            // ignore the changes then
            $this->updateStatus();

        }

        // unlock the file
        $this->unlockStorageFile();

        return true;

    }

    /**
     * Parses SQL and explodes individual queries.
     *
     * @param string $sql
     * @return array
     */
    protected function parseQueries($sql) {

        $queries = new SqlParser($sql);

        return $queries->explodeQueries();

    }

    /**
     * Executes an array of SQL queries.
     *
     * @param array $queries
     */
    protected function executeQueries($queries) {

        foreach($queries as $query){

            try {

                // execute the query
                $this->dibi->query($query['sql']);

                // save the current progress
                $this->updateStatus($this->status['bytes'] + $query['bytes']);

            } catch (\Dibi\DriverException $exc) {

                // is this a duplicate table problem on first run?
                if ($exc->getCode() == 1050 AND $this->firstInstall){
                    // jump to the end of the file
                    $this->updateStatus();
                }

                // forward the exception
                $this->exception($exc);

            }

        }

    }

    /**
     * Throws an exception if allowed or logs a warning silently.
     *
     * @param \Exception $exception
     * @return boolean
     * @throws \Exception
     */
    protected function exception($exception) {

        // notify the developer
        if ($this->silent){
            // fail silently with a warning only
            trigger_error ('DatabaseAutoinstaller SQL execution failed. ' . $exception->getMessage(), E_USER_WARNING);
            return false;
        }else{
            // we are allowed to throw an exception, be verbose
            $exception = new \Exception('DatabaseAutoinstaller SQL execution failed.', 1, $exception);
            throw $exception;
        }

    }

    /**
     * Check if the structure has changed
     *
     * @return boolean True if the structure file has changed since the last run, false otherwise.
     */
    protected function structureHasChanged() {

        // the file was not modified since the last update
        if (filemtime($this->structureFile) <= $this->status['last_update'] AND
            filesize($this->structureFile) == $this->status['bytes'])
            return false;

        return true;

    }

    /**
     * Get all new SQL appended to the structure file since the last update.
     *
     * @return string|boolean Appended SQL on success, false on failure.
     */
    protected function getStructureDifference() {

        // open the file
        if (!($file = fopen($this->structureFile, 'rb')))
            return false;

        // if the file has shrinked, return an empty string
        $filesize = filesize($this->structureFile);
        if ($filesize < $this->status['bytes'])
            return false;

        // skip to the new bytes
        fseek($file, $this->status['bytes']);

        // load the rest of the file
        $difference = fread($file, $filesize);
        $difference = $difference;

        // close the file
        fclose($file);

        return strlen($difference) ? $difference : false;

    }

    /**
     * Get a path to the storage file.
     *
     * @return string Path to the storage file.
     */
    protected function getStorageFileName(){

        // create the directory if it does not exist
        if (!file_exists($this->storage))
            mkdir($this->storage, 0777, true);

        $environment = preg_replace('[^a-zA-Z0-9\-_]', '', $this->environment);

        return $this->storage . '/' . $environment . '_' . $this->storageFile;

    }

    /**
     * Create an exclusive lock on the storage file.
     */
    protected function lockStorageFile() {

        // create the file if it does not exist yet
        $mode = file_exists($this->getStorageFileName()) ? 'r' : 'w';

        $file = fopen($this->getStorageFileName(), $mode);
        flock($file, LOCK_EX);
        fclose($file);

    }

    /**
     * Releast the lock on the storage file.
     */
    protected function unlockStorageFile() {

        $file = fopen($this->getStorageFileName(), 'r');
        flock($file, LOCK_UN);
        fclose($file);

    }

    /**
     * Update status with the current values.
     *
     * @param int $bytes Number of bytes parsed.
     */
    protected function updateStatus($bytes = null) {

        $this->status['last_update'] = time();
        $this->status['bytes'] = $bytes === null ? filesize($this->structureFile) : $bytes;
        $this->saveStatus();

    }

    /**
     * Save status variable to the storage file.
     */
    protected function saveStatus() {

        $encoded = json_encode($this->status);

        file_put_contents($this->getStorageFileName(), $encoded);
    }

    /**
     * Load staus variable from the storage file.
     *
     * @return array Status.
     */
    protected function loadStatus() {

        // set default status values, if the last status is not provided
        if (!file_exists($this->getStorageFileName())){
            $this->status = array('last_update' => 0, 'bytes' => 0);
            $this->firstInstall = true;
            return $this->status;
        }

        // load status and parse it
        $encoded = file_get_contents($this->getStorageFileName());
        $this->status = json_decode($encoded, true);

        // set default status values, if the last status is not provided
        if (!$this->status OR
            !isset($this->status['last_update']) OR
            !isset($this->status['bytes'])){
            $this->status = array('last_update' => 0, 'bytes' => 0);
            $this->firstInstall = true;
        }else{
            $this->firstInstall = false;
        }

        return $this->status;

    }

}