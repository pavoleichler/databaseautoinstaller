<?php

namespace PavolEichler;

/**
 * Parses SQL and explodes single queries.
 */
class SqlParser {

    const CHAR_EXECUTOR = 'e';
    const CHAR_QUOTE = 'q';
    const CHAR_COMMENT = 'c';

    protected $sql;

    protected $special = array(
        'comments' => array(
            array('/*', '*/'),
            array('#', "\n")
        ),
        'quotes' => array(
            '"', '`', '\''
        ),
        'escapes' => array(
            '\\'
        ),
        'executors' => array(
            ';'
        )
    );

    /**
     *
     * @param string $sql
     */
    public function __construct($sql) {

        $this->sql = $sql;

    }

    /**
     * Explodes to an array of individual queries.
     *
     * @return array
     */
    public function explodeQueries() {

        preg_match_all($this->getRegexPattern(), $this->sql, $matches, PREG_OFFSET_CAPTURE);

        // create an array of queries
        $queries = array();

        // keep track about commented and quoted parts
        $comment = false;
        $quoted = false;
        // build a single query SQL
        $sql = "";
        // track current offset
        $processedOffset = 0;
        foreach($matches[0] as $order => $match){
            // get special character data
            $character = $matches[2][$order][0];
            $offset = $matches[2][$order][1];
            // check if it is escaped
            $escaped = ($matches[1][$order][1] != -1);

            switch ($this->getType($character)) {
                case self::CHAR_COMMENT:
                    // start comment
                    if (!$comment AND !$quoted AND $this->isOpeningComment($character)){
                        $comment = $this->getCommentType ($character);
                        // save all the SQL up to this point
                        $sql .= substr($this->sql, $processedOffset, $offset - $processedOffset);
                        $processedOffset = $offset;
                    }
                    // stop comment
                    if ($comment AND $this->isClosingComment($character) AND $this->getCommentType($character) == $comment){
                        $comment = false;
                        // skip to the end and ignore content
                        $processedOffset = $offset + strlen($character) /* ignore the ending comment characters as well*/;
                    }
                    break;
                case self::CHAR_QUOTE:
                    // start quoted
                    if (!$quoted AND !$comment)
                        $quoted = $this->getQuoteType($character);
                    // stop quoted
                    if ($quoted AND !$escaped AND $this->getQuoteType($character) == $quoted)
                        $quoted = false;
                    break;
                case self::CHAR_EXECUTOR:
                    if (!$quoted AND !$comment){
                        // save all the SQL up to this point
                        $sql .= substr($this->sql, $processedOffset, $offset - $processedOffset + strlen($character) /* use the executor character as well */);
                        $processedOffset = $offset + strlen($character);
                        // add to the list of queries
                        $queries[] = array(
                            'sql' => $sql,
                            'bytes' => $offset + strlen($character)
                        );
                        // reset
                        $sql = "";
                    }
                    break;
            }

        }

        return $queries;

    }

    /**
     * Creates a regex pattern to find all special SQL characters.
     *
     * @return string
     */
    protected function getRegexPattern() {

        $pattern = "";
        $escapePattern = "";

        // escapes
        foreach($this->special['escapes'] as $escape){
            $escapePattern .= "$escape|";
        }
        $escapePattern = substr($escapePattern, 0, -1);

        // comments
        foreach($this->special['comments'] as $comments){
            foreach($comments as $comment)
                $pattern .= "$comment|";
        }

        // quotes
        foreach($this->special['quotes'] as $quote){
            $pattern .= "$quote|";
        }

        // executors
        foreach($this->special['executors'] as $executor){
            $pattern .= "$executor|";
        }
        $pattern = substr($pattern, 0, -1);

        // escape
        $pattern = $this->escapeRegex($pattern);
        $escapePattern = $this->escapeRegex($escapePattern);

        return "/($escapePattern)?($pattern)/";

    }

    /**
     * Escapes special regex chracters.
     *
     * @param string $string
     * @return string
     */
    protected function escapeRegex($string) {

        return str_replace(array('\\', '*', '.', '/', '(', ')', "\n", "\t"), array('\\\\', '\\*', '\\.', '\\/', '\\(', '\\)', '\n', '\t'), $string);

    }

    /**
     * Returns the type of the special chracter or false.
     *
     * @param string $character
     * @return mixed
     */
    public function getType($character) {

        if ($this->isExecutor($character))
            return self::CHAR_EXECUTOR;

        if ($this->isQuote($character))
            return self::CHAR_QUOTE;

        if ($this->isComment($character))
            return self::CHAR_COMMENT;

        return false;

    }

    /**
     * Checks if the provided character is a query separator, usually a semicolon.
     *
     * @param string $character
     * @return boolean
     */
    public function isExecutor($character) {

        return in_array($character, $this->special['executors']);

    }

    /**
     * Checks if the provided character is a qoute.
     *
     * @param string $character
     * @return boolean
     */
    public function isQuote($character) {

        return in_array($character, $this->special['quotes']);

    }

    /**
     * Checks if the provided character is an opening or closing comment.
     *
     * @param string $character
     * @return boolean
     */
    public function isComment($character) {

        foreach($this->special['comments'] as $comments){
            if (in_array($character, $comments))
                return true;
        }

        return false;

    }

    /**
     * Checks if the provided character is an opening comment.
     *
     * @param string $character
     * @return boolean
     */
    public function isOpeningComment($character) {

        foreach($this->special['comments'] as $comments){
            if ($character == reset($comments))
                return true;
        }

        return false;

    }

    /**
     * Checks if the provided character is a closing comment.
     *
     * @param string $character
     * @return boolean
     */
    public function isClosingComment($character) {

        foreach($this->special['comments'] as $comments){
            if ($character == end($comments))
                return true;
        }

        return false;

    }

    /**
     * Returns the type of the provided comment character.
     *
     * @param string $character
     * @return boolean
     */
    public function getCommentType($character) {

        foreach($this->special['comments'] as $type => $comments){
            if (in_array($character, $comments))
                return $type;
        }

        return false;

    }

    /**
     * Returns the type of the provided quote character.
     *
     * @param string $character
     * @return boolean
     */
    public function getQuoteType($character) {

        foreach($this->special['quotes'] as $type => $quote){
            if ($character == $quote)
                return $type;
        }

        return false;

    }

}